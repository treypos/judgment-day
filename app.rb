@human = 1000
@machine = 1000

def luck? 
    rand(0..1) == 1
end

def boom 
    diff = rand(1..10)
    if luck?
    @machine -= diff
    puts "#{diff} машин уничтожено"
    else
    @human -= diff
     puts "#{diff} людей погибло"
    end
end

def random_city 
    dice = rand(1..5)
    if dice == 1
        'Москва'
    elsif dice == 2
        'Казань'
    elsif dice == 3
        'Санкт-Петербург'
    elsif dice == 4
        'Екатеринбург'
    else dice == 5 
        'Нижний Новгород'
    end
end

def random_sleep 
    sleep rand(0.3..1.5)
end

def stats 
    puts "Осталось #{@human} людей и #{@machine} машин"
end

def event1
    puts "Запущена ракета по городу #{random_city}"
    random_sleep
    boom
end

def event2
    puts "Атомную бомбу запустили в #{random_city}"
    random_sleep
    boom
end

def event3
    puts "Беспилотники полетели в #{random_city}"
    random_sleep
    boom
end

def event4
    puts "Группа солдат прорывает оборону в городе #{random_city}"
    random_sleep
    boom
end

def event5
    puts "Танки атаковали город #{random_city}"
    random_sleep
    boom
end



def check_victory?
    if @human <= 0
        puts "machine win"
    true
    elsif
        @machine <=0
        puts "human win"
    true
    end
end

loop do 
if check_victory?
exit
end
dice = rand(1..5)
if dice == 1 
event1
elsif dice == 2 
event2
elsif dice == 3
event3
elsif dice == 4
event4
else dice == 5
event5
end
stats
random_sleep
end